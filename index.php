<?php

require_once __DIR__.'/vendor/autoload.php';


use Illuminate\Database\Capsule\Manager as DB;
use adelars\controleur\ControleurCategorie;
use adelars\controleur\ControleurItem;
use adelars\controleur\ControleurUser;
use adelars\controleur\ControleurReservation;

$db = new DB();
$config = parse_ini_file("conf/conf.ini");
$db->addConnection($config);
$db->setAsGlobal();
$db->bootEloquent();
session_start();
$app = new \Slim\Slim();

$app->get("/",function(){
    $v=new \adelars\vue\VuePage();
    echo  $v->render(1);
})->name("acceuil");

$app->get("/categorie",function(){
   $c=new ControleurCategorie();
   $c->categorie();
})->name("cate");

$app->get("/categorie/:no",function($no){
    $c=new ControleurCategorie();
    $c->categorieNo($no);
})->name("numcate");

$app->get("/item/:id",function($id){
    $i=new ControleurItem();
    $i->item($id);
})->name("item");

$app->post("/item/:id",function($id){
    $app=\Slim\Slim::getInstance();
    if(isset($_POST["valider"],$_POST["comm"])) {

        $i = new ControleurItem();
        $i->addCom($_POST["comm"],$id);
    }
    $app->redirect($app->urlFor("item",array("id"=>$id)));
});

$app->get("/listeUser",function(){
    $c = new ControleurUser();
    $c->listUserExistant();
})->name("listeUsers");


$app->get("/listeUserConex",function(){
    $c = new ControleurUser();
    $c->connectrapide();
})->name("conex");

$app->get("/connexionrapide/:id",function($id){
    $app = \Slim\Slim::getInstance();
    $c=new ControleurUser();
    $c->loaduser($id);
    $app->redirect($app->urlFor("acceuil"));
})->name("conrap");

$app->get("/connexion",function(){
   $v=new \adelars\vue\VuePage();
   echo $v->render(2);
})->name("connexion");

$app->post("/connexion", function () {
    $app= \Slim\Slim::getInstance();
    if(isset($_POST['valider_inc']) && $_POST['valider_inc']="validate_f1"){
        if(ControleurUser::authentificate(filter_var($_POST['name'], FILTER_SANITIZE_SPECIAL_CHARS),filter_var($_POST['pwd'], FILTER_SANITIZE_SPECIAL_CHARS)))
            $app->redirect($app->urlFor('acceuil'));
        else $app->redirect($app->urlFor('connexion'));
    }

});
$app->get("/inscription",function(){
    $v=new \adelars\vue\VuePage();
    echo $v->render(3);
})->name("inscription");

$app->post("/inscription",function() {
    $app = \Slim\Slim::getInstance();
    if (isset($_POST['valider_inc']) && $_POST['valider_inc'] = "validate_f1" && isset($_POST['name'])) {
        ControleurUser::createUser(filter_var($_POST['name'], FILTER_SANITIZE_SPECIAL_CHARS), filter_var($_POST['pwd'], FILTER_SANITIZE_SPECIAL_CHARS));
    }
    $app->redirect($app->urlFor('acceuil'));
});

$app->get("/modification",function(){
    $v=new \adelars\vue\VuePage();
    echo $v->render(5);
})->name("modification");

$app->post("/modification",function() {
    $app = \Slim\Slim::getInstance();
    if (isset($_POST['valider_name']) && $_POST['valider_inc'] = "validate_f1" && isset($_POST['name'])) {
        ControleurUser::changeName($_POST['name']);
    }
    if (isset($_POST['valider_pwd']) && $_POST['valider_inc'] = "validate_f1" && isset($_POST['pwd_old']) && isset($_POST['pwd_new'])) {
        ControleurUser::changePassword($_POST['pwd_old'],$_POST['pwd_new']);
        $app->redirect($app->urlFor('connexion'));
    }
    $app->redirect($app->urlFor('modification'));
});

$app->get("/reservation/:no", function($id){
    $r = new ControleurReservation();
    $r->reservation($id);
})->name("reserv");

$app->get("/reserver/:no", function($id){
    $v = new \adelars\vue\VuePage();
    echo $v->render(4);
})->name("afficherReserver");

$app->post("/reserver/:no", function($id){
    $app=\Slim\Slim::getInstance();
    if(null !== $_POST["valider"]){
        $r = new ControleurReservation();
        $r->reserver($id, $_POST['jour_debut'], $_POST['jour_fin'], $_POST['heure_debut'], $_POST['heure_fin']);
    }
    $app->redirect($app->urlFor("item", array("id" =>$id)));
});

$app->get("/deconexion",function(){
    $app= \Slim\Slim::getInstance();
    session_destroy();
    session_start();
    $app->redirect($app->urlFor('acceuil'));
})->name("deconnexion");

$app->get("/user/reservation/:id",function($id){
    $c=new ControleurUser();
    $c->afficheReservation($id);
})->name("reservuser");

$app->get("/item/reservation/:id",function($id){
    $c=new ControleurItem();
    $c->afficheReservation($id);
})->name("reserveitem");

$app->get("/gestion/reservation/",function(){
    $c=new ControleurReservation();
    $c->afficheReservation();
})->name("autoris");

$app->post("/gestion/reservation/:id",function($id){
    $app=\Slim\Slim::getInstance();
    if(null !== $_POST["autoriser"]){
        $r = new ControleurReservation();
        $r->autoriser($id);
    }
    $app->redirect($app->urlFor("autoris"));
});

$app->post("/paye/:id",function($id){
    $app=\Slim\Slim::getInstance();
    if(isset($_POST["paye"])){
        $r = new ControleurReservation();
        $r->payement($id);
    }
    $app->redirect($app->urlFor("reservuser",array("id"=>$_SESSION['id'])));
})->name("paye");

$app->run();