<?php
/**
 * Created by PhpStorm.
 * User: svzsv
 * Date: 08/02/2018
 * Time: 09:59
 */

namespace adelars\models;


class Commentaire extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "commentaire";
    protected $primaryKey = "id";
    public $timestamps=false;
}