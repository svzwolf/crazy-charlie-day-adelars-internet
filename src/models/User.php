<?php
/**
 * Created by PhpStorm.
 * User: svzsv
 * Date: 08/02/2018
 * Time: 10:02
 */

namespace adelars\models;


class User extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "user";
    protected $primaryKey = "id";
    public $timestamps=false;
}