<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 08/02/2018
 * Time: 14:13
 */

namespace adelars\models;


class Reservation extends \Illuminate\Database\Eloquent\Model{

    protected $table = "reservation";
    protected $primaryKey = "id_item";
    public $timestamps = true;


}