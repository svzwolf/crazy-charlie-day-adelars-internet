<?php
/**
 * Created by PhpStorm.
 * User: svzsv
 * Date: 08/02/2018
 * Time: 09:59
 */

namespace adelars\models;


class Categorie extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "categorie";
    protected $primaryKey = "id";
    public $timestamps=false;
}