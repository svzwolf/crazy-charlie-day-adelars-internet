<?php
/**
 * Created by PhpStorm.
 * User: svzsv
 * Date: 08/02/2018
 * Time: 10:01
 */

namespace adelars\models;


class Item extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "item";
    protected $primaryKey = "id";
    public $timestamps=false;
}