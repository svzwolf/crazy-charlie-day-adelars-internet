<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 08/02/2018
 * Time: 10:06
 */

namespace adelars\vue;


use Slim\Slim;

class VueCategorie extends VuePrincipale {

    private $tab;

    function __construct($tab){

        $this->tab = $tab;

    }

    private function listeCategorie(){
    $app=Slim::getInstance();
        $res = "<section>";
        $i=1;

        foreach ($this->tab[0] as $row){
        $res.="<div class='cate'>";
            $res .= "<a href='".$app->urlFor("numcate",array("no"=>$i))."'><h3>{$row['nom']} : {$row['description']}</h3> <br/><br/><img src='".$app->urlFor("acceuil")."img/item/".$this->tab[1][$i-1].".jpg'/></a></div>";
            $i++;
        }

        $res .= "</section>";

        return $res;

    }

    private function listeCategorieItem(){
        $app=Slim::getInstance();

        $res="";

        foreach($this->tab as $row){

            $res .= "<tr>
                         <td><center><a href='".$app->urlFor("item",array("id"=>$row['id']))."'><h3>{$row['nom']}  :</h3>  {$row['description']}</a></center></td>
                    </tr><hr/>";

        }

        $res .= '</table></section>';
        return $res;

    }

    function render($select){

        $res = "";
        switch ($select){

            case 1:
                $res .= $this->listeCategorie();
                break;
            case 2:
                $res .= $this->listeCategorieItem();
                break;
        }

        return parent::render($res);

    }

}