<?php
/**
 * Created by PhpStorm.
 * User: kylia
 * Date: 08/02/2018
 * Time: 11:24
 */

namespace adelars\vue;

use Slim\Slim;

class VueUsers extends VuePrincipale
{

        private $tab;

        function __construct($tableau){
            $this->tab = $tableau;
        }

        private function listeUser(){
            $app = Slim::getInstance();
            $res = "<section><p>";

            $res .="<center><h1>Voici les comptes rapides disponibles : </h1></center>";

            foreach ($this->tab as $row){
                $res .= "<div class='user'><center><p><a href ='".$app->urlFor("conrap",array("id"=>$row['id']))."'>{$row{'nom'}}</p>";
                $res .= "<img src='".$app->urlFor("acceuil")."img/user/".$row['id'].".jpg'> <hr/></center></div>";
            }

            $res .="</p></section>";

            return $res;
        }

        private function listeUserAll(){
            $res = "<section><p>";
            $app = Slim::getInstance();

            $res .="<center><h1>Voici les utilisateur déjà existant : </h1></center>";

            foreach ($this->tab as $row){
                $res .= "<div class='user'><center><h3>{$row['id']}. {$row{'nom'}}</h3>";

                $res .= "<img src='".$app->urlFor("acceuil")."img/user/".$row['id'].".jpg'> <p><a href ='".$app->urlFor("reservuser",array("id"=>$row['id']))."'>Plannings de {$row{'nom'}}</a></p><hr/></center></div>";
            }

            $res .="</p></section>";

            return $res;
        }

        private function reserv(){
            $app=Slim::getInstance();
            $res="<center><h1>Reservation de {$this->tab[0]['nom']} acceptée</h1>";
            foreach ($this->tab[1] as $row){
                $res.="<section>
                    <p>Jour et heure de début : ".$row['jour_debut']." : ".$row['heure_debut']."</p>
                    <p>Jour et heure de fin : ".$row['jour_fin']." : ".$row['heure_fin']."</p>
                    <p>Item no : ".$row['id_item']."</p>
                    <p>Etat de la reservation : ".$row['etat']."</p>
                    <p>Date de création : date de création</p>
                    <p>Date de modification : date de modification</p>
                </section><hr/>";
            }

            $res.="<hr/><h1>Reservation de {$this->tab[0]['nom']} en attente</h1>";
            foreach ($this->tab[2] as $row){
                $res.="<section>
                    <p>Jour et heure de début : ".$row['jour_debut']." : ".$row['heure_debut']."</p>
                    <p>Jour et heure de fin : ".$row['jour_fin']." : ".$row['heure_fin']."</p>
                     <p>Item no : ".$row['id_item']."</p>
                    <p>Etat de la reservation : ".$row['etat']."</p>";

                if(isset($_SESSION['id']))
                    if($_SESSION['id']==$this->tab[0]['id'] && $row['etat']==2)
                          $res.="<form method=\"POST\" action=\"{$app->urlFor('paye',array("id"=>$row['id']))}\"><input type='submit' name='paye' value='valider payement'/></form>";

                $res.="</section><hr/>";
            }
            $res.="</center>";
            return $res;
        }


        function render($select){
            $res="";
            switch($select){
                case 1:
                    $res .= $this->listeUser();
                    break;
                case 2:
                $res .= $this->listeUserAll();
                break;
                case 3:
                    $res .= $this->reserv();
            }

            return parent::render($res);
        }
}