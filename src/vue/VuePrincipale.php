<?php
/**
 * Created by PhpStorm.
 * User: svzsv
 * Date: 18/12/2017
 * Time: 11:25
 */

namespace adelars\vue;


use Slim\Slim;

class VuePrincipale
{
    public function render($select){
        $app=Slim::getInstance();
        $res = <<<END
<!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>crazy charlie day</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="manifest" href="site.webmanifest">
	<link rel="apple-touch-icon" href="icon.png">
    <link href="{$app->urlFor("acceuil")}css/main.css" rel="stylesheet" >
	
	
</head>
<body>

	<header class="header">

		<a href="#" class="nav__icon" id="nav__icon"></a>
		<nav class="nav">
			<a href="{$app->urlFor("acceuil")}">Accueil</a>
			<a href="{$app->urlFor("cate")}">Categorie </a>
            <a href="{$app->urlFor("listeUsers")}">Utilisateurs </a>
END;
        if(!isset($_SESSION["id"]))
			$res.="<a href='{$app->urlFor('conex')}'>Connexion rapide</a>
			<a href='{$app->urlFor('connexion')}'>Connexion</a>
			<a href='{$app->urlFor('inscription')}'>Inscription</a>";
        else{
            $res.="<a href='{$app->urlFor('modification')}'>Modification</a>";
            $res.="<a href='{$app->urlFor('deconnexion')}'>Deconnexion</a>";
            if($_SESSION['level']>1) $res.="<a href='{$app->urlFor('autoris')}'>Autorisation</a>";}


        $res .= <<<END
		</nav>
	</header>
	 <div id="container">
	 $select
    </div>

		
		
		<div class="site-cache" id="site-cache">
		
		</div>


		<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
		<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
		

		<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
		<script>
			window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
			ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
		</script>
		<script src="https://www.google-analytics.com/analytics.js" async defer></script>
		<script src="/js/responsiveNavbar.js"></script>

	</body>
	</html>
END;
       
       return $res;
    }
}