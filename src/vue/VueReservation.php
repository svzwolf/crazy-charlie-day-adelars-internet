<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 08/02/2018
 * Time: 11:49
 */

namespace adelars\vue;
use Slim\Slim;
use adelars\models\User as User;

class VueReservation extends VuePrincipale{

    private $tab;

    function __construct($tab){

        $this->tab = $tab;

    }

    private function afficherReservation(){

        return "<section>
                    <p>Jour et heure de début : ".$this->tab['jour_debut']." : ".$this->tab['heure_debut']."</p>
                    <p>Jour et heure de fin : ".$this->tab['jour_fin']." : ".$this->tab['heure_fin']."</p>
                    
                    <p>Etat de la reservation : ".$this->tab['etat']."</p>
                    <p>Date de création : date de création</p>
                    <p>Date de modification : date de modification</p>
                </section>";

    }

    private function afficherReservationNC(){
        $app = Slim::getInstance();
        $res = "<section>";

        foreach ($this->tab as $row){
            $res .= "<img src='".$app->urlFor("acceuil")."img/item/".$row['id'].".jpg'> <hr/></center></div>";
            $res.="<p>Jour de départ : ".$row['jour_debut']. "à Jour de fin : ".$row['jour_fin']."</br>
                      Heure de départ : ".$row['heure_debut']. "à Heure de fin : ".$row['heure_fin']."</br>";
            $perso = User::all()->where("id","=",$row['id_user'])->first();
            $res .="Réserver par". $perso;
        }

    }

    private function reserv(){
        if($_SESSION['level']>1){
        $res="<center><h1>Reservation à accepter</h1>";
        foreach ($this->tab[0] as $row){
            $res.="<section>
                    <p>Jour et heure de début : ".$row['jour_debut']." : ".$row['heure_debut']."</p>
                    <p>Jour et heure de fin : ".$row['jour_fin']." : ".$row['heure_fin']."</p>
                    <p>Item no : ".$row['id_item']."</p>
                    <p>Etat de la reservation : ".$row['etat']."</p>
                   
                   <form method=\"POST\" action=\"{$_SERVER['REQUEST_URI']}{$row['id']}\"><input type='submit' name='autoriser'/></form>
                </section><hr/>";
        }
        $res.="</center>";
        return $res;}
        else return "";
    }

    function render($select){

        $res = "";
        switch ($select){

            case 1:
                $res .= $this->afficherReservation();
                break;
            case 2:
                $res .= $this->afficherReservationNC();
                break;

            case 3:
                $res .= $this->reserv();
                break;

        }

        return parent::render($res);

    }

}