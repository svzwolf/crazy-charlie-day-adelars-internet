<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 08/02/2018
 * Time: 10:05
 */

namespace adelars\vue;


use adelars\models\User;
use Slim\Slim;

class VueItem extends VuePrincipale {

    private $tab;

    function __construct($tab){

        $this->tab = $tab;

    }

    private function item(){
        $app=Slim::getInstance();
        $item = $this->tab[0];
        $comm = $this->tab[1];
        $res ="<section>
                    <center>
                        <h1>".$item['nom']."</h1>".$item['description']."
                        <br/>
                        <img src='".$app->urlFor("acceuil")."img/item/".$item['id'].".jpg'>
                        <br/>             
                    <a href = ".$app->urlFor("reserveitem",array("id"=>$item['id']))."><input type = 'button' value = 'Voir réservation'></a>
                    
                    <a href = ".$app->urlFor("afficherReserver",array("no"=>$item['id']))."><input type = 'button' value = 'Réserver'></a> 
                    <h2>Commentaire :</h2>";
        foreach ($comm as $row){
            $res.="<p>".$row[1]."  :  ".$row[0]."</p>";
        }
          $res.=     "<h3>Ajouter un commentaire</h3>
        <form name=\"commentaire\" method=\"POST\" action=".$_SERVER['REQUEST_URI'].">
                        <input type=\"textarea\" name=\"comm\"/>
                        <input type=\"submit\" name=\"valider\" value=\"OK\"/>
                    </form>
</center>
        </section>";
    return $res;
    }

    private function reserv(){
        $res="<center><h1>Reservation de {$this->tab[0]['nom']} acceptée</h1>";
        foreach ($this->tab[1] as $row){
            $res.="<section>
                    <p>Jour et heure de début : ".$row['jour_debut']." : ".$row['heure_debut']."</p>
                    <p>Jour et heure de fin : ".$row['jour_fin']." : ".$row['heure_fin']."</p>
                    <p>Etat de la reservation : ".$row['etat']."</p>
                </section><hr/>";
        }
        $res.="</center>";
        return $res;
    }

    function render($select){

        $res = "";
        switch ($select){

            case 1:
                $res .= $this->item();
                break;
            case 2:
                $res .= $this->listeCategorieItem();
                break;
            case 3:
                $res .= $this->reserv();
                break;

        }

        return parent::render($res);

    }

}