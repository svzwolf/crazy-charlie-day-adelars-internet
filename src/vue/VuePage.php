<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 08/02/2018
 * Time: 10:05
 */

namespace adelars\vue;


use Slim\Slim;

class VuePage extends VuePrincipale {


    private function acceuil(){
        $app=Slim::getInstance();
        $rand=rand(1,2);
        switch ($rand){
            case 1:
                return "<center><img src='{$app->urlFor("acceuil")}img/garagesolidaire.png' id='logo'></center>";
                break;
            case 2:
                return "<center><img src='{$app->urlFor("acceuil")}img/garagesolidaire1.png' id='logo'></center>";
                break;
        }

    }

    private function connection(){
        return "<center><h3>Authentification</h3>
		<section>
			<h4>Connection<br></h4>
			<form id='form1' method='POST' action=\"{$_SERVER['REQUEST_URI']}\">
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Identifiant </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" class=\"form-control\" name=\"name\">
					</div>
				</div>
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Mot de passe </label>
					<div class=\"col-xs-9\">
						<input type=\"password\" class=\"form-control\" name=\"pwd\">
					</div>
				</div>
				<div class=\"form-group row\">
					<div class=\"offset-xs-3 col-xs-9\">
						<button type=\"submit\" name='valider_inc' value='valid_f1' class=\"btn btn-default\">S'inscrire</button>
					</div>
				</div>
			</form>
		</section></center>";
    }

    private function modification(){
        return "<center><h3>Modification</h3>
		<section>
			<h4>Nom<br></h4>
			<form id='form1' method='POST' action=\"{$_SERVER['REQUEST_URI']}\">
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Identifiant </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" class=\"form-control\" name=\"name\">
					</div>
				</div>
				
				<div class=\"form-group row\">
					<div class=\"offset-xs-3 col-xs-9\">
						<button type=\"submit\" name='valider_name' value='valid_f1' class=\"btn btn-default\">Modifier</button>
					</div>
				</div>
			</form>
			<h4>Mot de passe<br></h4>
			<form id='form1' method='POST' action=\"{$_SERVER['REQUEST_URI']}\">
			<div class=\"form - group row\">
					<label class=\"col - xs - 3 col - form - label mr - 2\"> Ancien mdp </label>
					<div class=\"col - xs - 9\">
						<input type=\"password\" class=\"form - control\" name=\"pwd_old\">
					</div>
				</div>
				
				<div class=\"form - group row\">
					<label class=\"col - xs - 3 col - form - label mr - 2\"> Nouveau mdp </label>
					<div class=\"col - xs - 9\">
						<input type=\"password\" class=\"form - control\" name=\"pwd_new\">
					</div>
				</div>
				<div class=\"form - group row\">
					<div class=\"offset - xs - 3 col - xs - 9\">
						<button type=\"submit\" name='valider_pwd' value='valid_f1' class=\"btn btn -default\">Modifier</button>
					</div>
				</div>
			</form>
		</section></center>";
    }

    private function inscription(){
        return "<center><h3>Authentification</h3>
		<section>
			<h4>Inscription<br></h4>
			<form id='form1' method='POST' action=\"{$_SERVER['REQUEST_URI']}\">
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Identifiant </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" class=\"form-control\" name=\"name\">
					</div>
				</div>
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Mot de passe </label>
					<div class=\"col-xs-9\">
						<input type=\"password\" class=\"form-control\" name=\"pwd\">
					</div>
				</div>
				<div class=\"form-group row\">
					<div class=\"offset-xs-3 col-xs-9\">
						<button type=\"submit\" name='valider_inc' value='valid_f1' class=\"btn btn-default\">S'inscrire</button>
					</div>
				</div>
			</form>
		</section></center>";
    }

    public function reserver(){

        return "<center><section>
                    <form name=\"réservation\" method=\"post\" action=".$_SERVER['REQUEST_URI'].">
                        Entrez un jour de début : <br/><input type=\"text\" name=\"jour_debut\"/><br/>
                        Entrez un jour de fin : <br/><input type=\"text\" name=\"jour_fin\"/><br/>
                        Entrez une heure de début : <br/><input type=\"text\" name=\"heure_debut\"/><br/>
                        Entrez une heure de fin : <br/><input type=\"text\" name=\"heure_fin\"/><br/>
                        <input type=\"submit\" name=\"valider\" value=\"OK\"/>
                    </form>
                </section></center>";

    }

    function render($select){

        $res = "";
        switch ($select){

            case 1:
                $res .= $this->acceuil();
                break;
            case 2:
                $res .= $this->connection();
                break;
            case 3:
                $res .= $this->inscription();
                break;
            case 4:
                $res .= $this->reserver();
                break;
            case 5:
                $res.= $this->modification();
                break;
        }

        return parent::render($res);

    }

}