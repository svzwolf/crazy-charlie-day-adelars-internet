<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 08/02/2018
 * Time: 14:14
 */

namespace adelars\controleur;


use adelars\models\Reservation;
use adelars\vue\VueReservation;

class ControleurReservation{

    public function reservation($id){

        $r = Reservation::where('id_item', '=', $id)->first()->toArray();
        $v = new \adelars\vue\VueReservation($r);
        echo $v->render(1);

    }
    public function afficheReservation(){

        $r=Reservation::all()->where("etat","=",0)->toArray();
        $v=new VueReservation(array($r));
        echo $v->render(3);
    }

    public function autoriser($id){
    $r=Reservation::all()->where("id","=",$id)->first();
    $r->etat=2;
    $r->save();
    }

    public function payement($id){
        $r=Reservation::all()->where("id","=",$id)->first();
        $r->etat=1;
        $r->save();
    }
    public function reserver($id, $jour_debut, $jour_fin, $heure_debut, $heure_fin){

        $user_id = $_SESSION['id'];

        if(isset($jour_debut) && isset($jour_fin) && isset($heure_debut) && isset($heure_fin)){

            $reservation = new Reservation();

            $reservation->id_item = $id;
            $reservation->id_user = $user_id;
            $reservation->jour_debut = filter_var($jour_debut, FILTER_SANITIZE_SPECIAL_CHARS);
            $reservation->jour_fin = filter_var($jour_fin, FILTER_SANITIZE_SPECIAL_CHARS);
            $reservation->heure_debut = filter_var($heure_debut, FILTER_SANITIZE_SPECIAL_CHARS);
            $reservation->heure_fin = filter_var($heure_fin, FILTER_SANITIZE_SPECIAL_CHARS);
            $reservation->etat = 0;

            $reservation->save();

        }

    }

}