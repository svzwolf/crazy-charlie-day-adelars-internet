<?php
/**
 * Created by PhpStorm.
 * User: kylia
 * Date: 08/02/2018
 * Time: 09:58
 */

namespace adelars\controleur;

use adelars\models\Item as Item ;
use adelars\models\Categorie as Categorie;
use adelars\models\User;


class ControleurCategorie
{
    public function categorie(){
        $c=Categorie::all()->toArray();
        $voiture=Item::where("id_categ","=",1)->count();
        $i1=Item::where("id_categ","=",1)->skip(rand(0,$voiture-1))->first()->id;

        $garage=Item::where("id_categ","=",2)->count();
        $i2=Item::where("id_categ","=",2)->skip(rand(0,$garage-1))->first()->id;

        $v=new \adelars\vue\VueCategorie(array($c,array($i1,$i2)));
        echo $v->render(1);
    }

    public function categorieNo($c){
        $c=Item::all()->where("id_categ","=",$c)->toArray();
        $v=new \adelars\vue\VueCategorie($c);
        echo $v->render(2);
    }
}