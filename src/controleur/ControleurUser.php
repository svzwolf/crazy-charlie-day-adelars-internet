<?php
/**
 * Created by PhpStorm.
 * User: kylia
 * Date: 08/02/2018
 * Time: 09:58
 */

namespace adelars\controleur;
use adelars\models\User as User;
use adelars\vue\VuePage;
use adelars\models\Reservation as Reservation;
use adelars\vue\VueReservation;
use adelars\vue\VueUsers as vueU;
use adelars\vue\VueUsers;

class ControleurUser
{
    public function listUserExistant(){
        $listUser=User::all()->toArray();
        $vue = new vueU($listUser);
        echo $vue->render(2);
    }

    public function connectrapide(){
        $listUser=User::all()->where("mdp","=",NULL)->toArray();
        $vue = new vueU($listUser);
        echo $vue->render(1);
    }

    public function afficheReservation($id){
        $user=User::all()->where("id","=",$id)->first()->toArray();
        $r=Reservation::all()->where("id_user","=",$id)->where("etat","=",1)->toArray();
        $rr=Reservation::all()->where("id_user","=",$id)->where("etat","!=",1)->toArray();
        $v=new VueUsers(array($user,$r,$rr));
        echo $v->render(3);
    }

    public static function createUser($username,$pwd)
    {
            $hash=password_hash($pwd,PASSWORD_DEFAULT,['cost'=> 12]);
            $user = new user();
            $user->id=NULL;
            $user->nom=$username;
            $user->mdp=$hash;
            $user->niveau=1;
            $user->save();
    }

    public static function authentificate($user,$pass){

        $u = User::where("nom", "=", $user)->first();
        if(!$u==null) {
            if (password_verify($pass, $u->mdp)) {
                self::loaduser($u->id);
            } else {
                return false;
            }
        }else{
            return false;
        }
        return true;
    }

    public static function loaduser($id)
    {
        if (isset($_SESSION)) {
            session_destroy();
        }
        session_start();
        $u = User::where("id", "=", $id)->first();
        $_SESSION['level'] = $u->level;
        $_SESSION['nom'] = $u->nom;
        $_SESSION['id'] = $u->id;
        if(isset($_COOKIE['user']))
            \Slim\Slim::getInstance()->setcookie('user',"");
        \Slim\Slim::getInstance()->setcookie('user',$u->user_id,time()+5e+6);
    }

    public static function checkPrivilege($value){
        if($value>$_SESSION['level'])return false;
        return true;
    }

    public static function changeName($name){
        $u = User::where("id", "=", $_SESSION['id'])->first();
        $u->nom=$name;
        $u->save();
    }

    public static function changePassword($oldpass,$pass){

        $u = User::where("id", "=", $_SESSION['id'])->first();
        if(!$u==null) {
            if (password_verify($oldpass, $u->mdp)) {
                $u->mdp=password_hash($pass,PASSWORD_DEFAULT,['cost'=> 12]);
                $u->save();
                session_destroy();
            } else {
                return false;
            }
        }else{
            return false;
        }
        return true;
    }

    public function validerReservation(){
        $res = Reservation::all()->where("etat","=",0)->toArray();
        if($_SESSION['niveau']>1){
            $vue = new VueReservation($res);
            echo $vue->render(2);
        }
    }
}