<?php
namespace adelars\controleur;

use adelars\models\Commentaire;
use adelars\models\Item;
use adelars\models\Reservation;
use adelars\models\User;
use adelars\vue\VueItem;

class ControleurItem{

	
	public function item($id) {
		$i=Item::where('id', '=', $id)->first()->toArray();
		$com=Commentaire::all()->where('id_item',"=",$id)->toArray();
		$ret=array();
		foreach ($com as $row){
		    $u=User::where("id","=",$row["id_user"])->first()->nom;
		    $ret[]=array($row["comm"],$u);
        }
		$tab=[$i,$ret];
		$v=new \adelars\vue\VueItem($tab);
        echo $v->render(1);
	}

    public function afficheReservation($id){
        $user=Item::all()->where("id","=",$id)->first()->toArray();
        $r=Reservation::all()->where("id_item","=",$id)->where("etat","=",1)->toArray();
        $v=new VueItem(array($user,$r));
        echo $v->render(3);
    }

	public function addCom($com,$id)
    {
        $commentaire = new Commentaire();
        $commentaire->id_user = $_SESSION['id'];
        $commentaire->id_item = $id;
        $commentaire->comm = filter_var($com,FILTER_SANITIZE_SPECIAL_CHARS);
        $commentaire->save();
    }
}